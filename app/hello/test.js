﻿define([],
    function () {
        var test = function () {
            var self = this;

            var defaultViewHtml = '<div> <h1>Hello World</h1></div>';
            var currentView = null;

            self.getView = function () {
                console.log('GetView');
                if (!currentView) {
                    currentView = $(defaultViewHtml)[0];
                }
                return currentView;
            };

            self.activate = function (activateOptions) {
                console.log('Activate');
            };

            self.attached = function (view, parent, settings) {
                console.log('Attatched');
            };

            self.detached = function (view, parent) {
                console.log('Detatched');
            };
        };

        return test;
    });